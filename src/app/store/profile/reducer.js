function MsgReducer(
	state = {
		isAuthenticated: false,
	},
	action
) {
	switch (action.type) {
	default:
		return state;
	}
}

export default MsgReducer;
